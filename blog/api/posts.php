<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/blog/doutor/_app/Config.inc.php";
if (isset($_POST['token']) && $_POST['token'] === TOKEN_API) {
    unset($_POST['token']);
    $post = $_POST;
    var_dump($post);
    $file = ['gallery_file' => [0 => $_FILES['gallery_file']], 'blog_cover' => $_FILES['blog_cover']];
    $posts = array_merge($post, $file);
    $blog = new Blog();
    $blog->ExeCreate($posts);
    echo "Post criado com sucesso!";
} else {
    header('HTTP/1.1 401 Unauthorized');
    echo "Acesso negado. Token inválido.";
}