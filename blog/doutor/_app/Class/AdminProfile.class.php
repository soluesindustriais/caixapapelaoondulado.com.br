<?php
/**
* AdminProfile.class [ CLASSE ]
* Classe responsabel pela administração de perfis do usuário logado.
* @copyright (c) 2016, Rafael da Silva Lima - Inove Dados
*/
class AdminProfile {
    private $Data;
    private $User;
    private $Error;
    private $Result;
//Id da empresa para montagem da pasta de upload
    private $Empresa;
/**
* <b>Atualizar Usuário:</b> Envelope os dados em uma array atribuitivo e informe o id de um
* usuário para atualiza-lo no sistema!
* @param INT $UserId = Id do usuário
* @param ARRAY $Data = Atribuitivo
*/
public function ExeUpdate($UserId, array $Data) {
    $this->User = (int) $UserId;
    $this->Data = $Data;
    $this->Empresa = (int) $this->Data['empresa_id'];
    unset($this->Data['empresa_id']);
    $this->CheckUnset();
    $this->checkData();
    if ($this->Result):
        $this->Update();
    endif;
}
/**
* <b>Verificar Cadastro:</b> Retorna TRUE se o cadastro ou update for efetuado ou FALSE se não.
* Para verificar erros execute um getError();
* @return BOOL $Var = True or False
*/
public function getResult() {
    return $this->Result;
}
/**
* <b>Obter Erro:</b> Retorna um array associativo com um erro e um tipo.
* @return ARRAY $Error = Array associatico com o erro
*/
public function getError() {
    return $this->Error;
}
########################################
########### METODOS PRIVADOS ###########
########################################
//Verifica os dados digitados no formulário
//Metodo para verificar se os campos opcionais estão vazios e dar unset em sua chave no array.
private function CheckUnset() {
    if (empty($this->Data['user_password']) && empty($this->Data['txtConfirmPassword'])):
        unset($this->Data['user_password'], $this->Data['txtConfirmPassword']);
endif;
if (empty($this->Data['user_cover'])):
    unset($this->Data['user_cover']);
endif;
if (empty($this->Data['user_about'])):
    $this->Data['user_about'] = ' ';
endif;
if (empty($this->Data['user_fone'])):
    $this->Data['user_fone'] = ' ';
endif;
if (empty($this->Data['user_ramal'])):
    $this->Data['user_ramal'] = ' ';
endif;
}
private function checkData() {
    if (in_array('', $this->Data)):
        $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ALERT, "MPI Technology");
        $this->Result = false;
    elseif (isset($this->Data['user_password']) && !empty($this->Data['user_password']) && empty($this->Data['txtConfirmPassword'])):
        $this->Error = array("Confirme sua senha para realizar as alterações.", WS_ALERT, "MPI Technology");
    $this->Result = false;
elseif (!$this->CheckPass()):
    $this->Error = array("As senhas não coincidem.", WS_ALERT, "MPI Technology");
    $this->Result = false;
elseif (!Check::Email($this->Data['user_email'])):
    $this->Error = array("Digite um endereço de e-mail válido.", WS_ALERT, "MPI Technology");
    $this->Result = false;
elseif (isset($this->Data ['user_password']) && (strlen($this->Data ['user_password']) < 6 || strlen($this->Data['user_password']) > 30)):
    $this->Error = array("A senha deve ter entre 6 e 30 caracteres.", WS_INFOR, "MPI Technology");
$this->Result = false;
else:
    $this->Data['user_nasc'] = Check::Data($this->Data['user_nasc']);
    $this->checkEmail();
endif;
}
//Verifica se as senhas são iguais
private function CheckPass() {
    if ($this->Data['user_password'] != $this->Data['txtConfirmPassword']):
        return false;
    else:
        unset($this->Data['txtConfirmPassword']);
        return true;
    endif;
}
//Verifica usuário pelo e-mail, Impede cadastro duplicado!
private function checkEmail() {
    $Where = ( isset($this->User) ? "user_id != {$this->User} AND" : '');
    $readUser = new Read;
    $readUser->ExeRead(TB_USERS, "WHERE {$Where} user_email = :email", "email={$this->Data['user_email']}");
    if ($readUser->getRowCount()):
        $this->Error = array("Digite um endereço de e-mail diferente.", WS_ERROR, "MPI Technology");
        $this->Result = false;
    else:
        $this->sendFoto();
    endif;
}
//Verifica e envia a foto do perfil do usuário para pasta da (int) empresa/user
private function sendFoto() {
    if (!empty($this->Data['user_cover']['tmp_name'])):
        $this->checkCover();
        $Upload = new Upload;
        $ImgName = "user-{$this->Data['user_name']}-" . (substr(md5(time() + $this->Data['user_lastname']), 0, 5));
        $Upload->Image($this->Data['user_cover'], $ImgName, 300, null, 'users');
        if ($Upload->getError()):
            $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
            $this->Result = false;
        else:
            $this->Data['user_cover'] = $Upload->getResult();
            $this->Result = true;
        endif;
    else:
        $this->Result = true;
    endif;
}
//Verifica se já existe uma foto, se sim deleta para enviar outra!
private function checkCover() {
    $readCapa = new Read;
    $readCapa->ExeRead(TB_USERS, "WHERE user_id = :id", "id={$this->User}");
    $capa = $readCapa->getResult();
    if ($readCapa->getResult()):
        $delCapa = $capa[0]['user_cover'];
        if (file_exists("uploads/{$delCapa}") && !is_dir("uploads/{$delCapa}")):
            unlink("uploads/{$delCapa}");
    endif;
endif;
}
//Atualiza Usuário!
private function Update() {
    $Update = new Update;
    if (isset($this->Data['user_password'])):
        $this->Data['user_password'] = md5($this->Data['user_password']);
    endif;
    $Update->ExeUpdate(TB_USERS, $this->Data, "WHERE user_id = :id", "id={$this->User}");
    if ($Update->getResult()):
        $this->Error = array("O usuário <b>{$this->Data['user_name']}</b> foi atualizado com sucesso.", WS_ACCEPT, "MPI Technology");
        $this->Result = true;
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Perfil", "Atualizou o perfil.", date("Y-m-d H:i:s"));
    endif;
}
}