<div class="page-container">
    <div class="page-header clearfix">
        <div class="row">
            <?php
            $lv = 2;
            if (!APP_USERS || empty($userlogin) || $user_level < $lv):
                die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "SIGA"));
            endif;
            ?>
            <div class="col-sm-6">
                <h4 class="mt-0 mb-5">Cadastro de notas</h4>
                <ol class="breadcrumb mb-0">
                    <li><a href="painel.php">SIGA</a></li>   
                    <li><a href="javascript:;">Notas</a></li>                     
                    <li class="active">Cadastrar</li>
                </ol>
            </div>
        </div>
    </div>

    <form id="form-vertical" method="post" novalidate="novalidate" enctype="multipart/form-data">
        <div class="page-content container-fluid">
            <div class="widget">
                <div class="widget-heading clearfix">
                    <h3 class="widget-title pull-left">Cadastro de notas</h3>
                    <div class="pull-right">
                        <button type="submit" name="CadastraNotas" class="btn btn-primary"><i class="ti-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=notas/index'"><i class="ti-share-alt"></i></button>
                    </div>
                </div>
                <div class="widget-body">
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['CadastraNotas'])):
                        unset($post['CadastraNotas']);
                        $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
                        $post['notas_user'] = $_SESSION['userlogin']['user_id'];

                        $cadastra = new AdminNotas;
                        $cadastra->ExeCreate($post);

                        if (!$cadastra->getResult()):
                            WSErro($cadastra->getError()[0], $cadastra->getError()[1], null, $cadastra->getError()[2]);
                        else:
                            $_SESSION['Error'] = $cadastra->getError();
                            header('Location: painel.php?exe=notas/create&get=true');
                        endif;
                    endif;

                    if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;
                    ?>   

                    <div class="form-group">
                        <label for="Titulo">Título</label>
                        <input id="Titulo" type="text" name="notas_titulo" value="<?php
                        if (isset($post['notas_titulo'])): echo $post['notas_titulo'];
                        endif;
                        ?>" placeholder="Digite um título para a nota" data-rule-required="true" data-rule-rangelength="[1,80]" class="form-control">
                    </div> 

                    <div class="form-group">
                        <label for="Mensagem">Mensagem</label>
                        <textarea name="notas_msg" id="Mensagem" placeholder="Digite a mensagem da nota" rows="3" class="form-control"><?php
                            if (isset($post['notas_msg'])): echo $post['notas_msg'];
                            endif;
                            ?></textarea>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>