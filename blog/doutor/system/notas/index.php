<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-comment"></i> Notas cadastradas</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="x_title">
                            <h2>Confira abaixo as notas que foram deixadas no sistema.</h2>
                            <div class="clearfix"></div>
                        </div>
                        <br/>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    $ReadNotas = new Read;
                    $ReadNotas->ExeRead(TB_NOTAS, "WHERE user_empresa = :emp ORDER BY notas_date DESC", "emp={$_SESSION['userlogin']['user_empresa']}");
                    if (!$ReadNotas->getResult()):
                        WSErro("Nenhuma nota foi encontrada.", WS_INFOR, null, "Aviso!");
                    else:
                        foreach ($ReadNotas->getResult() as $notas):
                            $ReadNotas->ExeRead(TB_USERS, 'WHERE user_id = :id', "id={$notas['notas_user']}");
                            if ($ReadNotas->getResult()):
                                foreach ($ReadNotas->getResult() as $us):
                                    extract($us);
                                    ?>
                                    <ul>
                                        <li class="media j_item" id="<?= $notas['notas_id']; ?>">
                                            <div class="media-left avatar" style="width: 100px; height: 100px;">
                                                <?= Check::Image('../doutor/uploads/' . $empresa_capa, $empresa_name, '', 100, 100); ?>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"><?= $user_name . ' ' . $user_lastname; ?></h6>
                                                <h5 class="title text-primary"><?= $notas['notas_titulo']; ?></h5>
                                                <p class="summary text-justify col-sm-11"><?= $notas['notas_msg']; ?></p>
                                            </div>
                                            <div class="media-right text-nowrap">
                                                <time datetime="<?= $notas['notas_date']; ?>" class="fs-11"><?= date("d/m/Y H:i", strtotime($notas['notas_date'])); ?></time>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>