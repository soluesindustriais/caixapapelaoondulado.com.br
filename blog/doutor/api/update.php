<?php
header('Content-Type: application/json');
define('IS_API', true);

// Inclua o arquivo de configuração
$configPath = '../_app/Config.inc.php';


if (file_exists($configPath)) {
    require_once $configPath;
} else {
    http_response_code(500); // Erro interno do servidor
    echo json_encode(['error' => 'Arquivo de configuração não encontrado!']);
    exit;
}

// Verifica o método de requisição
$method = $_SERVER['REQUEST_METHOD'];

if ($method === 'POST') { // usando POST para testar

    // Captura os dados diretamente de $_POST quando é enviado via form-data
    $postId = $_POST['blog_id'] ?? null;  // O ID da postagem deve ser um valor numérico, não um array
    $token = $_POST['token'] ?? null;     // Captura o token via form-data

    // Verifica se o blog_id foi passado corretamente
    if (is_null($postId) || !is_numeric($postId)) {
        http_response_code(400); // Requisição inválida
        echo json_encode(['error' => 'ID da postagem (blog_id) inválido ou não informado.']);
        exit;
    }

    // Verifica se o token é válido
    if ($token !== TOKEN_API) {
        http_response_code(401); // Não autorizado
        echo json_encode(['error' => 'Acesso negado. Token inválido.']);
        exit;
    }

    // Aqui você pode capturar outros dados da postagem e armazená-los em um array
    $blogData = [
        'blog_title' => $_POST['blog_title'] ?? null,
        'blog_id' => $postId ?? null,
        'blog_content' => $_POST['blog_content'] ?? null,
        'cat_parent' => $_POST['cat_parent'] ?? null,
        'user_id' => $_POST['user_id'] ?? null,
        'blog_status' => $_POST['blog_status'] ?? null,
        // Outros campos que você deseja atualizar
    ];

    // Verifica se há dados para atualização
    if (empty($blogData['blog_title']) && empty($blogData['blog_content']) && empty($blogData['blog_status'])) {
        http_response_code(400); // Requisição inválida
        echo json_encode(['error' => 'Nenhum dado fornecido para atualização.']);
        exit;
    }

    // Captura arquivo se necessário
    if (!empty($_FILES['blog_cover']['name'])) {
        $file = $_FILES['blog_cover'];
        // Aqui você pode processar o upload do arquivo
        // $blogData['blog_cover'] = processed file path here
    }

    // Tenta atualizar a postagem
    try {
        $blog = new Blog();
        $blog->ExeUpdate($blogData); // Passa o ID como int e os dados como array

        if ($blog->getResult()) {
            http_response_code(200); // Sucesso
            echo json_encode(['success' => 'Postagem atualizada com sucesso!']);
        } else {
            http_response_code(500); // Erro interno do servidor
            echo json_encode(['error' => $blog->getError()]);
        }
    } catch (Exception $e) {
        http_response_code(500); // Erro interno do servidor
        echo json_encode(['error' => 'Erro ao atualizar postagem: ' . $e->getMessage()]);
    }

} else {
    // Se o método não for POST
    http_response_code(405); // Método não permitido
    echo json_encode(['error' => 'Método não permitido. Use POST.']);
}
