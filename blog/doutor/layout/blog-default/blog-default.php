<div id="blog">
      <section itemid="<?= RAIZ; ?>/blog" itemscope itemtype="http://schema.org/LiveBlogPosting">
      <div class="bread bread--default">
          <div class="wrapper">
            <div class="bread__row">
              <?php Check::SetBreadcrumb($arrBreadcrump); ?>
              <h1 class="bread__title"><?php Check::SetTitulo($arrBreadcrump, $URL); ?></h1>
            </div>
          </div>
        </div>
        <div class="wrapper">
          <?php

          $Read->ExeRead(TB_USERS, "WHERE user_status = :st", "st=0");
          $authors = $Read->getResult();

          $categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE);
          if (!$categ): require 'inc/blog-inc.php';
          else:
            $Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :stats AND cat_parent = :parent ORDER BY cat_date DESC", "stats=2&parent={$categ}");
            if (!$Read->getResult()):
              $Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :stats AND cat_id = :parent ORDER BY cat_date DESC", "stats=2&parent={$categ}");
              if (!$Read->getResult()):
                WSErro("Desculpe, mas não foi encontrando o conteúdo relacionado a esta página, volte mais tarde", WS_INFOR, null, "Aviso!");
              else:
                $category = $Read->getResult();
                $category = $category[0]; ?>
                <!-- SE ESTIVER SETADO PARA TRUE NA CLIENT.INC.PHP MOSTRA DESCRIÇÃO DA CATEGORIA  -->
                <?php if (CAT_CONTENT): ?>
                  <div class="category-content">
                    <?= $category['cat_content']; ?>
                  </div>
                <? endif;
              endif; ?>
              <div class="container">
                <div class="grid grid-col-3">
                  <?php
                  $Read->ExeRead(TB_BLOG, "WHERE cat_parent = :cat AND blog_status = :stats ORDER BY blog_id DESC", "cat={$categ}&stats=2");
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $blog):
                      extract($blog); ?>
                      <div class="blog-card">
                        <div class="blog-card__image">
                          <img class="blog-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$blog_cover?>" alt="<?=$blog_title?>" title="<?=$blog_title?>">
                        </div>
                        <div class="blog-card__info">
                          <h3 class="blog-card__title"><?= $blog_title; ?></h3>
                          <p class="blog-card__date"><i class="fa fa-calendar" aria-hidden="true"></i> <?= date("d/m/Y", strtotime($blog_date)); ?></p>
                          <div class="blog-card__author">
                            <i class="fa-solid fa-user"></i>
                            <?php 
                              $authorKey = array_search($user_id, array_column($authors, 'user_id'));
                              $itemAuthor = $authors[$authorKey]['user_name'];
                            ?>
                            <a href="<?=$url?>autor/<?= urlencode($itemAuthor) ?>" rel="nofollow" title="<?=$itemAuthor?>"><?=$itemAuthor?></a>
                          </div>
                          <?php if (strlen($blog_keywords) > 1) : ?>
                              <div class="blog-card__tags">
                                  <?php $blogTagList = explode(",", $blog_keywords);
                                  foreach ($blogTagList as $key => $item) : ?>
                                      <a href="<?=$url?>tags/<?=$item?>" title="<?=$item?>" rel="nofollow"><?= $item ?></a>
                                  <?php endforeach; ?>
                              </div>
                          <?php endif; ?>
                          <div class="blog-card__description">
                            <?php if(BLOG_BREVEDESC && isset($blog_brevedescription)):
                              echo $blog_brevedescription;
                            else: ?>
                              <p class="blog-card__content-text"><?= Check::Words($blog_content, 25) ;?></p>
                            <?php endif; ?>
                          </div>
                          <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>" class="blog-card__btn">Leia mais <i class="fa-solid fa-angles-right"></i></a>
                        </div>
                      </div>
                    <? endforeach;
                  endif; ?>
                </div>
              </div>
            <? else: ?>
              <div class="container">
                <div class="grid grid-col-2">
                  <?php
                  foreach ($Read->getResult() as $cat):
                    extract($cat); ?>
                    <div class="blog-card">
                      <div class="blog-card__image">
                        <?php
                        if (empty($cat_cover)): ?>
                            <img class="blog-card__cover" src="<?=RAIZ?>/doutor/images/default.png" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                        <? else: ?>
                          <img class="blog-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$cat_cover?>" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                        <? endif; ?>
                      </div>
                      <div class="blog-card__info">
                        <p class="blog__date"><i class="fa fa-calendar" aria-hidden="true"></i> <?= date("d/m/Y", strtotime($cat_date)); ?></p>
                        <h3 class="blog-card__title"><?= $cat_title; ?></h3>
                        <p class="blog-card__text"><?= $cat_description; ?></p>
                        <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>" class="blog-card__btn"><span>leia mais</span></a>
                      </div>
                    </div>
                  <?php endforeach;
                  $Read->ExeRead(TB_BLOG, "WHERE cat_parent = :cat", "cat={$categ}");
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $blog):
                      extract($blog); ?>
                      <div class="blog-card">
                        <div class="blog-card__image">
                          <img class="blog-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$blog_cover?>" alt="<?=$blog_title?>" title="<?=$blog_title?>">
                        </div>
                        <div class="blog-card__info">
                          <p class="blog__date"><i class="fa fa-calendar" aria-hidden="true"></i> <?= date("d/m/Y", strtotime($blog_date)); ?></p>
                          <h3 class="blog-card__title"><?= $blog_title; ?></h3>
                          <div class="blog-card__description">
                            <?php if(BLOG_BREVEDESC && isset($blog_brevedescription)):
                              echo $blog_brevedescription;
                            else: ?>
                              <p><?= Check::Words($blog_content, 25) ;?></p>
                            <?php endif; ?>
                          </div>
                          <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>" class="blog-card__btn"><span>leia mais</span></a>
                        </div>
                      </div>
                    <? endforeach;
                  endif; ?>
                </div>
              </div>
            <? endif;
          endif; ?>
          <div class="container"><? include('inc/social-media.php'); ?></div>
        </div> <!-- wrapper -->
        <div class="clear"></div>
      </section>
    </div>