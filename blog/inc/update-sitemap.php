<?php

function sitemapInsert($sitemapFile, $sitemapUrl) {
    $xmlString = file_get_contents($sitemapFile);
    
    if(empty($xmlString)){ return; }

    $sitemap = new SimpleXMLElement($xmlString);

    $url = $sitemap->addChild('url');
    $url->addChild('loc', $sitemapUrl);
    $url->addChild('lastmod', date('Y-m-d'));
    $url->addChild('changefreq', 'daily');
    $url->addChild('priority', '0.9');

    file_put_contents($sitemapFile, sitemapFormat($sitemap->asXML()));
}

function sitemapRemove($sitemapFile, $sitemapUrl) {
    $xmlString = file_get_contents($sitemapFile);

    if(empty($xmlString)){ return; }

    // Carrega o XML em um objeto SimpleXMLElement
    $sitemap = new SimpleXMLElement($xmlString);

    $namespaces = $sitemap->getNamespaces(true);
    $sitemap->registerXPathNamespace('ns', $namespaces[null]);
    $urlsToRemove = $sitemap->xpath('//ns:url[ns:loc="' . $sitemapUrl . '"]');
    foreach ($urlsToRemove as $urlToRemove) {
        $domNode = dom_import_simplexml($urlToRemove);
        if ($domNode !== false) {
            $domNode->parentNode->removeChild($domNode);
        }
    }

    file_put_contents($sitemapFile, sitemapFormat($sitemap->asXML()));
}

function sitemapFormat($xmlString) {
    $dom = new DOMDocument();
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($xmlString);
    return $dom->saveXML();
}

$sitemapFile = '../sitemap.xml'; //RAIZ
// $sitemapFile = 'sitemap.xml'; //SITEMAP DO DIRETÓRIO BLOG
$sitemapUrl = $url.'pagina-teste-4';

sitemapInsert($sitemapFile, $sitemapUrl);
sitemapRemove($sitemapFile, $sitemapUrl);

?>