<div class="container">
  <div class="wrapper">
    <?php
    // ID do produto
    $idProdutos = array('1', '2', '3', '4', '5', '6');
    
    $Read->ExeRead(TB_PRODUTO, "WHERE user_empresa = :emp AND prod_status = :st AND prod_id IN (".implode(',', $idProdutos).") ORDER BY field(prod_id,".implode(',', $idProdutos).")", "emp=" . EMPRESA_CLIENTE . "&st=2");
    if ($Read->getResult()): ?>
      <h2>CONFIRA NOSSOS PRODUTOS</h2>
      <div class="card-group">
        <?php
        foreach ($Read->getResult() as $prod_home):
          extract($prod_home); ?>
          <div class="card card--prod">
            <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $prod_name; ?>" title="<?= $prod_title; ?>">
              <?= Check::Image('doutor/uploads/' . $prod_cover, $prod_title, 'card__cover', 260, 260) ?>
              <h2 class="card__title"><?= $prod_title; ?></h2>
              <!-- SE ESTIVER SETADO PARA TRUE NA CLIENT.INC.PHP MOSTRA DESCRIÇÃO NA THUMB DO PRODUTO  -->
              <?php if (PROD_BREVEDESC): ?>
                <div class="card__description">
                  <?= $prod_brevedescription?>
                </div>
              <? endif; ?>
            </a>
          </div>
        <? endforeach; ?>
      </div>
    <? endif; ?>
  </div>
  <div class="clear"></div>
</div>