<form>
  <input type="hidden" class="j_url" value="<?=RAIZ?>/<?= $getURL; ?>"/>
</form>
<aside class="aside-sig">
  <?php include 'inc/sub-menu-aside.php'; ?>
  <div class="aside__contact">
    <?php foreach ($fone as $key => $value): ?>
      <?php if ($value[2] != 'fa fa-whatsapp'): ?>
        <a class="btn" rel="nofollow" title="Clique e ligue" href="tel:<?=$value[0].$value[1]?>">
          <i class="<?=$value[2]?>"></i> Clique e ligue!
        </a>
      <?php else: ?>
        <a class="btn" rel="nofollow" href="https://<?=(!$isMobile) ? 'web' : 'api'?>.whatsapp.com/send?phone=55<?=$value[0].str_replace('-', '', $value[1]);?>&text=<?=rawurlencode("Olá! Gostaria de mais informações sobre ".RAIZ."/".$getURL)?>" target="_blank" title="Whatsapp <?=$nomeSite?>">
          Orçamento por <i class="<?=$value[2]?>"></i>
        </a>
      <?php endif; ?>
      <?php if($key >= 2) break; ?>
    <?php endforeach; ?>
    <a class="btn btn_orc" href="<?=RAIZ?>/contato" title="Entre em contato" ><i class="fas fa-envelope"></i> Orçamento por E-mail</a>
  </div>

<?php 
    $Read->ExeRead(TB_BLOG, "WHERE blog_status = :stats AND blog_keywords IS NOT NULL AND blog_keywords <> ''", "stats=2");
    if ($Read->getResult()): ?>
      <div class="blog-tag-list blog-tag-list--aside">
        <?php 
          $tagList = $Read->getResult();
          $arrayTags = [];
          foreach($tagList as $key => $item): ?>
          <?php $itemTagList = explode(",", $item['blog_keywords']);
            foreach ($itemTagList as $key => $blogTag): 
              array_push($arrayTags, $blogTag);
            endforeach; ?>
          <?php endforeach;
          $arrayTags = array_unique($arrayTags);
          sort($arrayTags);
          foreach($arrayTags as $key => $item): ?>
            <a href="<?=$url?>tags/<?=$item?>" title="<?=$item?>" rel="nofollow"><?= $item ?></a>
          <?php endforeach; ?>
      </div>
<?php endif; ?>
</aside>