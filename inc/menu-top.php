<div class="logo-top">
  <a href="<?=$url?>" title="Início">
    <img src="imagens/img-home/logo.png" alt="Logo" title="Logo"></a>
</div>
<ul>
  <li><a class="nav-link nav-link-ltr" href="<?=$url?>" title="Página inicial"><span class="fa-icons-class"><i
          class="fa fa-home"></i></span>Início</a></li>
  <li><a class="nav-link nav-link-ltr" href="<?=$url?>sobre-nos"><span class="fa-icons-class"><i
          class="fa fa-user"></i></span>Sobre Nós</a></li>
  <li class="dropdown"><a href="<?=$url?>produtos" title="Produtos"><span class="fa-icons-class"><i
          class="fa fa-box-open"></i></span>Produtos</a>
    <ul class="sub-menu">
      <? include('inc/sub-menu.php');?>
    </ul>
  </li>
  <li><a class="nav-link nav-link-ltr" href="<?=$url?>blog"><span class="fa-icons-class"><i
          class="fa fa-book"></i></span>Blog</a></li>

  <!--<li class="dropdown"><a href="<?=$url?>informacoes" title="Informações"><span class="fa-icons-class"><i
          class="fa fa-info-circle"></i></span>Informações <span class="fa-icons-down"><i
          class="fa fa-angle-down"></i></span></a>
    <ul class="sub-menu sub-menu-mpi">
      <? include('inc/sub-menu-info.php');?>
    </ul>
  </li>-->