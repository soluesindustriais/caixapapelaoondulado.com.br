
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Embalagem para esfiha','Caixa para esfiha','Embalagem para esfiha aberta','Caixa para esfiha aberta','Caixa para esfiha atacado','Embalagem para esfiha individual','Caixa de esfiha personalizada','Caixa de esfiha branca','Embalagem para esfiha fechada','Caixa para esfiha preço','Caixa esfiha 30x30','Caixa de esfiha fotográfica','Caixas de papelão para esfihas','Comprar caixa esfiha','Comprar caixa para esfiha','Caixa de esfiha sp','Caixa grande de esfiha personalizada','Caixa grande para esfiha','Empresa de caixa de esfiha branca','Caixa branca esfiha','Comprar caixa esfiha personalizada','Caixa embalagem esfiha','Caixa de esfiha personalizada preço','Fornecedor de caixa para esfiha','Caixa branca para esfiha valor','Fornecedor de caixa de esfiha branca','Caixa de esfiha fotográfica preço','Caixa de esfiha personalizada sp','Caixa para esfiha valor','Caixas de papelão para esfihas sp','Venda de caixa de esfiha','Caixa esfiha 30x30 preço','Caixa de esfiha personalizada valor','Fornecedor de caixa para esfiha sp','Caixa branca para esfiha preço','Fornecedor de caixa de esfiha branca sp','Comprar caixa de esfiha fotográfica','Preço da caixa de esfiha personalizada','Comprar caixa grande para esfiha','Caixas de papelão pequena para esfihas','Embalagem para esfiha sp','Venda de caixa de esfiha sp','Embalagem para esfiha aberta preço','Comprar embalagem para esfiha individual','Embalagem para esfiha fechada sp'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "caixa-de-esfiha";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/caixa-de-esfiha/caixa-de-esfiha-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-14"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/caixa-de-esfiha/caixa-de-esfiha-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/caixa-de-esfiha/thumbs/caixa-de-esfiha-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>