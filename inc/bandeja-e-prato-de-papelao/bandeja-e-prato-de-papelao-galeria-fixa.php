
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Bandeja de papelão','Prato de papelão','Bandejas de papelão para festas','Prato para bolo de papelão','Pratinho de papelão','Bandeja de papelão para docinhos','Bandeja de papelão redonda','Bandeja de papel para bolo','Bandeja de papelão decorada','Bandeja feita de papelão','Bandeja de doces de papelão','Pratos descartáveis de papel','Bandeja de papelão dourada','Bandeja de festa de papelão','Prato para bolo descartável papelão','Bandeja de papelão laminado','Bandeja de papelão quadrada','Pratinho de papelão para festa','Prato de papelão laminado','Bandeja de bolo papelão','Bandeja de docinhos de papelão','Bandeja de papel laminado','Bandeja para doces feita de papelão','Bandeja de papel para doces','Bandeja de caixa de papelão','Bandeja de papelão para bolo redondo','Bandeja de aniversário de papelão','Pratos descartáveis de papelão','Bandeja redonda de papelão','Pratos de papel descartáveis','Bandeja feita com caixa de papelão','Bandeja de papelão para bolo preço','Prato de papelão preço','Bandejas laminadas de papelão','Bandeja papelão para doces','Bandeja de papelão para festa infantil','Bandeja provençal em papelão','Bandeja de papelão encapada','Bandeja para salgadinhos papelão','Bandeja de papelão nuvem','Bandeja de papelão para casamento','Bandeja de papelão com pérola','Bandeja de papelão número 6','Bandeja de papelão espelhado','Bandeja de papelão onde comprar'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "bandeja-e-prato-de-papelao";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/bandeja-e-prato-de-papelao/bandeja-e-prato-de-papelao-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-14"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/bandeja-e-prato-de-papelao/bandeja-e-prato-de-papelao-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/bandeja-e-prato-de-papelao/thumbs/bandeja-e-prato-de-papelao-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>