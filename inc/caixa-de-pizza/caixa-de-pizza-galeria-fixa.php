
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Caixa de pizza','Caixa de pizza quadrada','Caixa de pizza atacado','Fábrica de caixa de pizza','Caixa para entregar pizza','Caixa oitavada','Caixa para entrega de pizza','Caixa de pizza 30 cm','Papelão para caixa de pizza','Caixa de pizza 25 cm','Caixa de pizza 35cm preço','Caixa de pizza preço','Caixa de pizza oitavada atacado','Caixa pizza oitavada','Caixa de papelão para pizza','Chapa de papelão para fazer caixa de pizza','Caixa de pizza brotinho','Caixa de pizza laminada','Caixa de pizza 40cm','Caixa de pizza valor','Caixa de pizza decorada','Caixa de pizza 45 cm','Caixa de pizza quadrada atacado','Caixa de pizza grande','Caixa de pizza 50 cm','Caixa de pizza 20 cm','Onde comprar caixa de pizza','Caixa de pizza barata','Caixa de pizza gigante','Caixa de pizza hexagonal','Caixa de pizza molde','Caixa de pizza no forno','Caixa de pizza comprar','Caixa de pizza redonda preço','Caixa de pizza unidade','Caixa de pizza fotográfica','Caixa de pizza para revenda','Caixa oitavada para pizza','Caixa de pizza oitavada preço','Papelão ondulado para caixa de pizza','Caixa de pizza personalizada','Caixa de papelão pizza oitavada','Caixa de pizza térmica','Chapa de papelão para pizza','Caixa de pizza sextavada'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "caixa-de-pizza";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/caixa-de-pizza/caixa-de-pizza-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-14"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/caixa-de-pizza/caixa-de-pizza-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/caixa-de-pizza/thumbs/caixa-de-pizza-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>