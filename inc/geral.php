<?
$nomeSite = 'Portal das Caixas';
$slogan = 'Simplesmente o melhor do ramo!';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
  $url = $http . "://" . $host . "/";
} else {
  $url = $http . "://" . $host . $dir["dirname"] . "/";
}


$emailContato = 'everton.lima@solucoesindustriais.com.br';
$rua = 'Rua Pequetita, 179';
$bairro = 'Vila Olimpia';
$cidade = 'São Paulo';
$UF = 'SP';
$cep = 'CEP: 04552-060';
$latitude = '-22.546052';
$longitude = '-48.635514';
$idAnalytics = 'UA-119867186-28';
$senhaEmailEnvia = '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode = explode("/", $_SERVER['PHP_SELF']);
$urlPagina = end($explode);
$urlPagina = str_replace('.php', '', $urlPagina);
$urlPagina == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
$tagsanalytic = ["G-JX904D9BR0","G-D23WW3S4NC"];
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';


//Breadcrumbs
include ('inc/categoriasGeral.php');
$caminho = '
	<div class="breadcrumb" id="breadcrumb  ">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="' . $url . '" itemprop="item" title="Home">
			<span itemprop="name"> Home   </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">' . $h1 . '</span>
		  <meta itemprop="position" content="2" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	  ';

    $caminho2 = '
    <div class="breadcrumb" id="breadcrumb">
                <div class="wrapper">
                    <div class="bread__row">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"> Home   </span>
                  </a>
                  <meta itemprop="position" content="1" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
                    <span itemprop="name">Produtos   </span>
                  </a>
                  <meta itemprop="position" content="2" />
                </li>
                <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                  <span itemprop="name">' . $h1 . '</span>
                  <meta itemprop="position" content="4" />
                </li>
              </ol>
            </nav>
            </div>
            </div>
            </div>
            ';

$caminhobandeja_e_prato_de_papelao = '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="' . $url . 'bandeja-e-prato-de-papelao-categoria" itemprop="item" title="Bandeja-e-prato-de-papelao - Categoria">
                      <span itemprop="name">Bandeja e prato de papelao - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">' . $h1 . '</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhocaixa_de_esfiha = '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="' . $url . 'caixa-de-esfiha-categoria" itemprop="item" title="Caixa-de-esfiha - Categoria">
                      <span itemprop="name">Caixa de esfiha - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">' . $h1 . '</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhocaixa_de_papelao = '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="' . $url . 'caixa-de-papelao-categoria" itemprop="item" title="Caixa-de-papelao - Categoria">
                      <span itemprop="name">Caixa de papelao - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">' . $h1 . '</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhocaixa_de_pizza = '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="' . $url . 'caixa-de-pizza-categoria" itemprop="item" title="Caixa-de-pizza - Categoria">
                      <span itemprop="name">Caixa de pizza - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">' . $h1 . '</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhocaixa_para_bolo = '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="' . $url . 'caixa-para-bolo-categoria" itemprop="item" title="Caixa-para-bolo - Categoria">
                      <span itemprop="name">Caixa para bolo - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">' . $h1 . '</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoformsubmit = '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="' . $url . '" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="' . $url . 'formsubmit-categoria" itemprop="item" title="Formsubmit - Categoria">
                      <span itemprop="name">Formsubmit - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">' . $h1 . '</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminho_caixa_de_papelao = '
<div class="breadcrumb" id="breadcrumb">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"> Home   </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
        <span itemprop="name">Produtos   </span>
      </a>
      <meta itemprop="position" content="2" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
    <a  href="' . $url . 'caixa-de-papelao-categoria" itemprop="item" title="Caixa de Papelão - Categoria">
      <span itemprop="name">Caixa de Papelão - Categoria   </span>
    </a>
    <meta itemprop="position" content="3" />
  </li>
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name">' . $h1 . '</span>
      <meta itemprop="position" content="4" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
';

$caminhomaquina_seladora_de_papelao = '
<div class="breadcrumb" id="breadcrumb">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"> Home   </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
        <span itemprop="name">Produtos   </span>
      </a>
      <meta itemprop="position" content="2" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
    <a  href="' . $url . 'maquina-seladora-de-papelao-categoria" itemprop="item" title=""Máquina Seladora de Caixa de Papelão
" - Categoria">
      <span itemprop="name">Máquina Seladora de Caixa de Papelão - Categoria   </span>
    </a>
    <meta itemprop="position" content="3" />
  </li>
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name">' . $h1 . '</span>
      <meta itemprop="position" content="4" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
';





?>