
    <link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
    <ul class="thumbnails-mod17">
        <?php $lista = array('');
        $extensions = ['jpg', 'png', 'jpeg', 'webp'];
        foreach ($extensions as $ext) {
            if(file_exists("imagens/maquina-seladora-de-papelao/thumbs/maquina-seladora-de-papelao-$i.$ext")){
                $extension = $ext;
                break;
            }         
        }
        shuffle($lista);
        for ($i = 1; $i < 13; $i++) { ?> <li> <a class="lightbox" href="<?= $url; ?>imagens/maquina-seladora-de-papelao/maquina-seladora-de-papelao-<?= $i ?>.<?=$extension?>" title="<?= $lista[$i] ?>"><img src="<?= $url; ?>imagens/maquina-seladora-de-papelao/thumbs/maquina-seladora-de-papelao-<?= $i ?>.<?=$extension?>" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" /></a> </li> <?php } ?></ul>
    