
    <div class="grid">
    <div class="col-6">
    <? $extensions = ['jpg', 'png', 'jpeg', 'webp'];
        foreach ($extensions as $ext) {
            if(file_exists("imagens/maquina-seladora-de-papelao/thumbs/maquina-seladora-de-papelao-$i.$ext")){
                $extension = $ext;
                break;
            }         
        } ?>
        <div class="picture-legend picture-center">
            <a href="<?= $url ?>imagens/maquina-seladora-de-papelao/maquina-seladora-de-papelao-01.<?= $extension ?>" class="lightbox" title="<?= $h1 ?>" target="_blank">
                <img src="<?= $url ?>imagens/maquina-seladora-de-papelao/thumbs/maquina-seladora-de-papelao-01.<?= $extension ?>" alt="<?= $h1 ?>" title="<?= $h1 ?>" />
            </a>
            <strong>Imagem ilustrativa de <?= $h1 ?></strong>
        </div>
    </div>
    <div class="col-6">
        <div class="picture-legend picture-center">
            <a href="<?= $url ?>imagens/maquina-seladora-de-papelao/maquina-seladora-de-papelao-02.<?= $extension ?>" class="lightbox" title="<?= $h1 ?>" target="_blank">
                <img src="<?= $url ?>imagens/maquina-seladora-de-papelao/thumbs/maquina-seladora-de-papelao-02.<?= $extension ?>" alt="<?= $h1 ?>" title="<?= $h1 ?>" />
            </a>
            <strong>Imagem ilustrativa de <?= $h1 ?></strong>
        </div>
    </div>
    </div>
    
