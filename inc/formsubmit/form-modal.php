<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!-- The Modal -->
<div id="myModal" class="box-form" onload='showProdutosForm()'>

    <!-- Modal content -->
    <div class="box-form-content">
        <span class="close-form">&times;</span>

        <form action="https://formsubmit.co/leadssolucs@gmail.com" method="post">

            <h2 class="" id="modal-title">Cote grátis agora!</h2>
            <p class="hide-desk">Além de receber um orçamento, você também poderá esclarecer suas dúvidas referentes ao assunto. </p>

            <b id="campo-obrigatorio">* Campo obrigatório</b>



            <br>


            <section class="input-fields">

                <input type="text" name="nome" placeholder="Nome *" required>
                <input type="text" id="email-modal" name="email" placeholder="Email *" required>

                <div class="dual-input-fields">

                    <div class="dual-input">
                        <input type="tel" id="fone-modal" name="telefone" placeholder="Telefone *" required>
                        <input type="tel" id="cell-modal" name="celular" placeholder="Celular *" required>
                    </div>
                </div>

                <div class="special-settings-dual" id="special-settings">

                    <div class="special-settings" id="">
                        <b id="dimensoes">Lote Mínimo</b>
                        <div class="dual-input-fields">

                            <select name="Região de Atendimento" id="regiao-select" required>
                                <option value="1000 Unidades">1000 Unidades</option>
                                <option value="Mais de 10K">Mais de 1000 Unidades</option>
                            </select>

                        </div>
                    </div>

                </div>

                <textarea name="mensagem" cols="30" rows="4" placeholder="Mensagem *" required></textarea>

                <div class="g-recaptcha" data-sitekey="6LfXlB8iAAAAAGUuTZxjx9WEf9i-tj_OyeEvt7gt"></div>

                <section class="btn-field">
                    <button type="submit" id="btn-modal" class="btn btn-primary btn-block font-weight-bold py-3 d-flex justify-content-center align-items-center btnSubmit">
                        Enviar Orçamento
                    </button>
                </section>

                <p class="italic-info">Insira as medidas desejadas e receba exatamente o produto que procura</p>


                <input type="hidden" name="_cc" value="leadssolucs@gmail.com">

                <!-- Evitar Spam -->
                <input type="text" name="_honey" style="display:none">

                <!-- Capturando tamanho da tela -->
                <input type="hidden" id="screen-size-two" name="tamanho da tela" value="">

                <!-- Capturando página origem da mensagem -->
                <input type="hidden" id="lead-origin-two" name="página de origem">

                <!-- Capurar categoria -->
                <input type="hidden" id="categoria" name="categoria" value="Caixas de Papelão">

                <!-- Página destino após forumulário ser preenchido -->
                <input id="next-page-two" type="hidden" name="_next" value="<?= $url ?>obrigado">
                <input type="hidden" name="_captcha" value="false">
                <input type="hidden" name="_autoresponse" value="Obrigado por enviar seu Email para o Soulções Industriais, logo responderemos sua mensagem!, Obrigado pelo contato!">


            </section>

        </form>

    </div>

</div>