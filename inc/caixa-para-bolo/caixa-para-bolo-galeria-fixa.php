
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Caixa para bolo','Caixa para bolo alta','Caixas para mini bolo','Caixa de papelão para bolo','Caixa para bolo personalizada','Caixa para transportar bolo','Prato de papelão para bolo','Bandeja de papelão para bolo','Caixas para bolos altos','Caixas para bolos e tortas','Caixa para bolo com alça','Caixa para bolo no pote','Caixa para bolo caseirinho','Caixa para bolo surpresa','Caixa para bolo transparente','Caixa para bolo alto personalizada','Caixas para bolo gelado','Caixas para bolos e doces','Caixa para bolo acetato','Caixa para bolo de andar','Caixa para bolo grande','Caixa para transportar bolo alto','Caixa para mini bolo 20x20x20','Caixa para bolo em fatias','Caixa para bolo de papelão','Caixa para bolo pequeno','Caixa para bolo branca','Caixa para bolo com tampa','Caixa para bolo fatiado','Caixa para bolo de aniversário','Caixa para bolo caseiro','Caixa para bolo 40x40','Caixas personalizadas para bolos e doces','Caixa para bolo embrulhado','Onde comprar caixa para bolo','Caixa para bolo confeitado','Caixa para mini bolo 12x12','Caixa para mini bolo 10x10x10','Caixa para transportar bolo de andares','Caixa para bolo preço','Caixa para transportar bolo grande','Caixas para tortas e bolos personalizadas','Caixas para bolos e salgados','Caixa para bolo em pedaços','Caixa para bolo número 9'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "caixa-para-bolo";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/caixa-para-bolo/caixa-para-bolo-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-14"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/caixa-para-bolo/caixa-para-bolo-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/caixa-para-bolo/thumbs/caixa-para-bolo-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>