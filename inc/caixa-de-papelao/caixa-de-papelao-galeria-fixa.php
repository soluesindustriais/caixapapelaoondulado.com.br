
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Caixa de papelão','Papelão ondulado','Caixa de papelão grande','Caixa de papelão pequena','Caixa de papelão com tampa','Caixa de papelão preço atacado','Comprar caixas de papelão para mudança','Caixa de papelão onde comprar','Caixa de papelão sob medida','Caixa de papelão grande para mudança','Chapa de papelão ondulado','Caixa papelão para mudança','Caixa de papelão preço','Caixa de papelão ondulado','Fabricante de papelão ondulado','Caixa de papelão com divisórias','Caixa de papel ondulado','Caixa de papelão com logo','Caixa de papelão embalagem','Venda de caixa de papelão para mudança','Comprar caixas para mudança','Caixa de papelão para comprar','Caixa de papelão reforçada','Caixa de papelão para transporte','Caixa de papelão venda','Caixa de papelão para cesta básica','Caixa sob medida','Papelão ondulado preço','Caixa de papelão maleta','Caixa de papelão tipo maleta','Caixa embalagem papelão','Caixa de papelão 1 metro','Indústria de papelão ondulado','Atacado de caixa de papelão','Caixa de papelão onda dupla','Caixa de papelão com colmeia','Chapa de papelão micro ondulado','Embalagem papelão ondulado','Caixa de papelão pequena preço','Caixa de papelão personalizada preço','Caixa de papelão sob encomenda','Caixa de papelão com tampa comprar','Empresas de papelão ondulado em sp','Caixa de papelão a pronta entrega','Fábrica de caixa de papelão ondulado'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "caixa-de-papelao";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/caixa-de-papelao/caixa-de-papelao-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-03-14"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/caixa-de-papelao/caixa-de-papelao-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/caixa-de-papelao/thumbs/caixa-de-papelao-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>